// Read and parse input
const fs = require('fs');
const input = fs
  .readFileSync('day1/input.txt', 'utf8')
  .split('\n')
  .map((n) => parseInt(n));

const sumIncreases = (input) => {
  const increases = input.reduce(
    (increases, depth, i, depths) =>
      i === 0 ? increases : depth > depths[i - 1] ? increases + 1 : increases,
    0
  );
  console.log(increases);
};

const calcWindowSums = (input) => {
  const windowSums = input.reduce(
    (sums, depth, i, depths) =>
      i > depths.length - 3
        ? sums
        : [...sums, depths[i] + depths[i + 1] + depths[i + 2]],
    []
  );
  return windowSums;
};

sumIncreases(calcWindowSums(input));
