// Read and parse input
const fs = require('fs');
const input = fs
  .readFileSync('day2/input.txt', 'utf8')
  .split('\n')
  .map((command) => {
    const [heading, distance] = command.split(' ');
    return [heading, parseInt(distance)];
  });

const executeCommand = ({ aim, horizontal, depth }, [heading, distance]) => {
  switch (heading) {
    case 'forward':
      return {
        aim,
        horizontal: horizontal + distance,
        depth: depth + aim * distance,
      };
    case 'down':
      return { aim: aim + distance, horizontal, depth };
    case 'up':
      return { aim: aim - distance, horizontal, depth };
  }
};

const { aim, horizontal, depth } = input.reduce(
  (position, command) => executeCommand(position, command),
  { aim: 0, horizontal: 0, depth: 0 }
);

console.log(aim, horizontal, depth, horizontal * depth);
